

// A functional component using the Hook API
import { useState } from 'react';
export function HelloWorld() {

  let [message, setMessage] = useState('Hello World');

  function reverse() {
    const newMessage = message.split('').reverse().join('');
    setMessage(newMessage);
  }

  return (
    <div>
      <h1 className="hello-world">{message}</h1>
      <button onClick={reverse}>Reverse!</button>
    </div>
  );

}


// // A class component (legacy)
// import { Component } from 'react';
// export class HelloWorld extends Component {
//
//     state = {
//         message: 'Hello World'
//     };
//
//     reverse = () => {
//         const newMessage = this.state.message.split('').reverse().join('');
//         this.setState({message: newMessage})
//     };
//
//     render() {
//         return (
//             <div>
//                 <h1 className="hello-world">{this.state.message}</h1>
//                 <button onClick={this.reverse}>Reverse!</button>
//             </div>
//         );
//     }
// }

