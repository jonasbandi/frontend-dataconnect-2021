import {customElement, html, LitElement, property} from 'lit-element';

@customElement('vw-greeter')
export class HelloWorldView extends LitElement {

  @property()
  message = '';

  render() {
    return html`
      <div>Greeter works: ${this.message}</div>
    `;
  }
}
