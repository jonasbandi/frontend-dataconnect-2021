import { showNotification } from '@vaadin/flow-frontend/a-notification';
import '@vaadin/vaadin-button';
import '@vaadin/vaadin-text-field';
import {css, customElement, html, LitElement, property, PropertyValues} from 'lit-element';
import '@vaadin/vaadin-button/src/vaadin-button.js';

import './greeter';

@customElement('hello-world-view')
export class HelloWorldView extends LitElement {
  @property()
  name: string = '';

  @property()
  message = 'Gugus';

  static get styles() {
    return css`
      :host {
        display: block;
        padding: 1em;
      }
    `;
  }
  render() {
    return html`
<vaadin-text-field label="Your name" @value-changed="${this.nameChanged}" .value="${this.name}"></vaadin-text-field>
<vaadin-button @click="${this.sayHello}">
  Say hello 
</vaadin-button>
<vaadin-button>
  Button 
</vaadin-button>
<div style="width: 100%; height: 100%;">
  Hello my name is: ${this.name} 
</div>
<div>
    <vw-greeter message="${this.message}"></vw-greeter>
</div>
`;
  }
  nameChanged(e: CustomEvent) {
    this.name = e.detail.value;
  }

  sayHello() {
    showNotification('Hello ' + this.name);
  }

  protected firstUpdated(_changedProperties: PropertyValues) {
    super.firstUpdated(_changedProperties);
    setInterval(() => {
        this.name = new Date().toISOString();
        this.message = new Date().toISOString();
    }, 1000);
  }
}
